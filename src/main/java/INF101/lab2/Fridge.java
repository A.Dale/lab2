package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {


    static int max;
    ArrayList<FridgeItem> fridgeItems;
    
    public Fridge(){
        fridgeItems = new ArrayList<> ();
        max = 20;
    }
    @Override
    public int nItemsInFridge() {
        int items = 0;
        for (int i = 0; i < fridgeItems.size(); i++) {
            if (fridgeItems != null){
                items++;
            }
        }
        return items;
    }

    @Override
    public int totalSize() {
        int max_size = 20;
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        fridgeItems.add(item);
        if (fridgeItems.size() <= 20){
            return true;
        }
        else return false;
        

    }

    @Override
    public void takeOut(FridgeItem item) {
          if (nItemsInFridge() == 0) {
              throw new NoSuchElementException();
          }

        else fridgeItems.remove(item);
            }
            
    
    
    

    @Override
    public void emptyFridge() throws NoSuchElementException {
        if(fridgeItems != null){
        fridgeItems.removeAll(fridgeItems);
        }
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> goodItems = new ArrayList<>();
        for(FridgeItem item : fridgeItems){
            if(item.hasExpired() == false){
                goodItems.add(item);
                String itemnameitem = item.getName();
                System.out.println(itemnameitem);
            }
            
        }
        fridgeItems.removeAll(goodItems);
        return fridgeItems;
    }
}




    
    

